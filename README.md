## Aggregate<>

The signature of the Aggregate method is the following:

	IEnumerable<T> Aggregate<T, TKey>(IEnumerable<T> data,
                                      Func<T, TKey> keySelector,
                                      Action<T, TKey> groupActions,
                                      Func<T, int> sumSelector,
                                      Action<T, int> sumAction,
                                      Func<T, double> avgSelector,
                                      Action<T, double> avgAction)
            where T : new()
			
where Func<T, TKey> keySelector - key selector for gropping;

Func<T, int> sumSelector - key selector for sum;

Func<T, double> avgSelector - key selector for avarage;

Actions here are methods wchich know how to assign value to the result oblect.

	magicAggregator.Aggregate(symbols,
                              g => g.Name,
                              (res, key) => res.Name = key,
                              s => s.Quantity,
                              (res, sum) => res.Quantity = sum,
                              a => a.Price,
                              (res, avg) => res.Price = avg);
                          
Honestly, I don't like this approach at all. 

It's not intuitive, generic code is bigger than not generic and this code would be really hard to maintain in production. 

Ideally I would like to have a method signature as:


	IEnumerable<T> Aggregate<T, TKey>(IEnumerable<T> data,
                                      Func<T, TKey> keySelector,
                                      Func<T, int> sumSelector,
                                      Func<T, double> avgSelector)
                                  
to be able to call this method as:


	magicAggregator.Aggregate(symbols,
                              g => { g.Name, g.Price },
                              s => s.Quantity,
                              a => { a.Price, a.Quantity });
          
                          
However the more I read the requirements, the more questions I have. 

The only suitable solution for me would be returning the anonymous type, but it does not meet the requirements.

By the way, I would be very thankful if you can provide a better solution as it's very interesting how to do this:)