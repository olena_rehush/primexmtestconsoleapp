﻿using PrimeXMTestConsoleApp.Helpers;
using System;
using System.Linq;
using Xunit;

namespace PrimeXMTestConsoleApp.UnitTests.Helpers
{
    public class SymbolsProviderTests
    {
        private readonly SymbolsProvider symbolsProvider;
        public SymbolsProviderTests()
        {
            symbolsProvider = new SymbolsProvider();
        }

        [Fact]
        public void GetSymbols_ZeroCount_ReturnsEmptyArray()
        {
            var result = symbolsProvider.GetSymbols(0);

            Assert.Empty(result);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(10)]
        [InlineData(100)]
        [InlineData(1000)]
        [InlineData(2000000)]
        public void GetSymbols_CountGreaterThanZero_ReturnsNotEmptyArray(int count)
        {
            var result = symbolsProvider.GetSymbols(count);

            Assert.NotEmpty(result);
            Assert.Equal(count, result.Count());
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-2)]
        [InlineData(-100)]
        [InlineData(-1000)]
        [InlineData(-2000000)]
        public void GetSymbols_CountLessThanZero_ThrowsException(int count)
        {
            Assert.Throws<ArgumentException>(() => symbolsProvider.GetSymbols(count));
        }
    }
}
