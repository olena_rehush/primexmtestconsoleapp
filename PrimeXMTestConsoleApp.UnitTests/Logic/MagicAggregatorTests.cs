﻿using PrimeXMTestConsoleApp.Logic;
using PrimeXMTestConsoleApp.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PrimeXMTestConsoleApp.UnitTests.Logic
{
    public class MagicAggregatorTests
    {
        private readonly MagicAggregator magicAggregator;

        public MagicAggregatorTests()
        {
            this.magicAggregator = new MagicAggregator();
        }

        [Fact]
        public void Aggregate_EmptyEnumerable_ReturnsEmptyEnumerable()
        {
            var list = Enumerable.Empty<Symbol>();

            var result = magicAggregator.Aggregate(list,
                                                   g => g.Name,
                                                   (res, key) => res.Name = key,
                                                   s => s.Quantity,
                                                   (res, sum) => res.Quantity = sum,
                                                   a => a.Price,
                                                   (res, avg) => res.Price = avg);

            Assert.Empty(result);
        }

        [Fact]
        public void Aggregate_NullEnumerable_ThrowsException()
        {
            List<Symbol> list = default;
            Assert.Throws<ArgumentNullException>(() =>
                   magicAggregator.Aggregate(list,
                                             g => g.Name,
                                             (res, key) => res.Name = key,
                                             s => s.Quantity,
                                             (res, sum) => res.Quantity = sum,
                                             a => a.Price,
                                             (res, avg) => res.Price = avg));
        }

        [Fact]
        public void Aggregate_ValidEnumerable_ReturnsAggregatedEnumerable()
        {
            var symbols = GetSymbols();

            var result = magicAggregator.Aggregate(symbols,
                                                   g => g.Name,
                                                   (res, key) => res.Name = key,
                                                   s => s.Quantity,
                                                   (res, sum) => res.Quantity = sum,
                                                   a => a.Price,
                                                   (res, avg) => res.Price = avg);

            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count());
            Assert.Equal(0, result.First().Id);
            Assert.Equal(0, result.Last().Id);
            Assert.Equal("EURUSD", result.First().Name);
            Assert.Equal("EURGBP", result.Last().Name);
            Assert.Equal(5, result.First().Quantity);
            Assert.Equal(4, result.Last().Quantity);
            Assert.Equal(1.565, result.First().Price);
            Assert.Equal(1.23, result.Last().Price);
        }

        private static List<Symbol> GetSymbols()
        {
            return new()
            {
                new Symbol
                {
                    Id = 1,
                    Name = "EURUSD",
                    Quantity = 2,
                    Price = 1.56
                },
                new Symbol
                {
                    Id = 2,
                    Name = "EURUSD",
                    Quantity = 3,
                    Price = 1.57
                },
                new Symbol
                {
                    Id = 3,
                    Name = "EURGBP",
                    Quantity = 4,
                    Price = 1.23
                }
            };
        }
    }
}
