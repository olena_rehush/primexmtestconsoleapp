﻿using PrimeXMTestConsoleApp.Types;
using System.Collections.Generic;

namespace PrimeXMTestConsoleApp.Helpers
{
    public interface ISymbolsDisplayer
    {
        void DisplaySymbols(IEnumerable<Symbol> symbols);
    }
}