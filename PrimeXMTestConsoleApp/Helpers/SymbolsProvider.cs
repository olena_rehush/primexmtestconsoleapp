﻿using PrimeXMTestConsoleApp.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PrimeXMTestConsoleApp.Helpers
{
    public class SymbolsProvider : ISymbolsProvider
    {
        private const string StringPool = "ab23";

        private readonly Random random;

        public SymbolsProvider()
        {
            random = new Random();
        }

        public IEnumerable<Symbol> GetSymbols(int count)
        {
            if (count < 0)
            {
                throw new ArgumentException($"Value {count} may not be less than 0");
            }

            return Enumerable.Range(0, count)
                .Select(item => new Symbol
                {
                    Id = random.Next(0, count * 2),
                    Name = GetRandomName(3),
                    Quantity = random.Next(0, 100),
                    Price = random.NextDouble()
                });

        }

        private string GetRandomName(int count)
        {
            var chars = Enumerable.Range(0, count)
                .Select(x => StringPool[random.Next(0, StringPool.Length)]);

            return new string(chars.ToArray());
        }
    }
}
