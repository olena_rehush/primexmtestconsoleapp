﻿using PrimeXMTestConsoleApp.Types;
using System;
using System.Collections.Generic;

namespace PrimeXMTestConsoleApp.Helpers
{
    public class SymbolsConsoleDisplayer : ISymbolsDisplayer
    {
        public void DisplaySymbols(IEnumerable<Symbol> symbols)
        {
            if (symbols is null)
            {
                throw new ArgumentNullException(nameof(symbols));
            }

            foreach (var (symbol, index) in symbols.WithIndex())
            {
                Console.WriteLine($"{index}: " +
                    $"{nameof(symbol.Id)}: {symbol.Id}, " +
                    $"{nameof(symbol.Name)}: {symbol.Name}, " +
                    $"{nameof(symbol.Quantity)}: {symbol.Quantity}, " +
                    $"{nameof(symbol.Price)}: {symbol.Price}.");
            }
        }
    }
}
