﻿using PrimeXMTestConsoleApp.Types;
using System.Collections.Generic;

namespace PrimeXMTestConsoleApp.Helpers
{
    public interface ISymbolsProvider
    {
        IEnumerable<Symbol> GetSymbols(int count);
    }
}
