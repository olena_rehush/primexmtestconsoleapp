﻿namespace PrimeXMTestConsoleApp.Types
{
    public class Symbol
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }
}
