﻿using PrimeXMTestConsoleApp.Helpers;
using PrimeXMTestConsoleApp.Logic;
using System;

namespace PrimeXMTestConsoleApp
{
    public class Demonstration
    {
        private readonly ISymbolsProvider symbolsProvider;
        private readonly IMagicAggregator magicAggregator;
        private readonly ISymbolsDisplayer symbolsDisplayer;

        public Demonstration(ISymbolsProvider symbolsProvider,
                             IMagicAggregator magicAggregator,
                             ISymbolsDisplayer symbolsDisplayer)
        {
            this.symbolsProvider = symbolsProvider ?? throw new ArgumentNullException(nameof(symbolsProvider));
            this.magicAggregator = magicAggregator ?? throw new ArgumentNullException(nameof(magicAggregator));
            this.symbolsDisplayer = symbolsDisplayer ?? throw new ArgumentNullException(nameof(symbolsDisplayer));
        }

        public void Run()
        {   
            var symbols = symbolsProvider.GetSymbols(10);

            Console.WriteLine("Initial data:");
            symbolsDisplayer.DisplaySymbols(symbols);

            var aggregatedSymbols = magicAggregator.Aggregate(symbols,
                                                              g => g.Name,
                                                              (res, key) => res.Name = key,
                                                              s => s.Quantity,
                                                              (res, sum) => res.Quantity = sum,
                                                              a => a.Price,
                                                              (res, avg) => res.Price = avg);

            Console.WriteLine("Aggregated data:");
            symbolsDisplayer.DisplaySymbols(aggregatedSymbols);
        }
    }
}
