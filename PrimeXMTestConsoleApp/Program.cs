﻿using Microsoft.Extensions.DependencyInjection;
using PrimeXMTestConsoleApp.Helpers;
using PrimeXMTestConsoleApp.Logic;
using System;

namespace PrimeXMTestConsoleApp
{
    class Program
    {
        private static IServiceProvider serviceProvider;

        static void Main(string[] args)
        {
            RegisterServices();
            var scope = serviceProvider.CreateScope();

            scope.ServiceProvider.GetRequiredService<Demonstration>().Run();

            DisposeServices();
        }

        private static void RegisterServices()
        {
            var services = new ServiceCollection()
                .AddTransient<ISymbolsProvider, SymbolsProvider>()
                .AddTransient<IMagicAggregator, MagicAggregator>()
                .AddTransient<ISymbolsDisplayer, SymbolsConsoleDisplayer>()
                .AddTransient<Demonstration>();

            serviceProvider = services.BuildServiceProvider(true);
        }

        private static void DisposeServices()
        {
            if (serviceProvider == null)
            {
                return;
            }
            if (serviceProvider is IDisposable)
            {
                ((IDisposable)serviceProvider).Dispose();
            }
        }
    }
}
