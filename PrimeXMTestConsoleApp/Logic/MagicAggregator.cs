﻿using PrimeXMTestConsoleApp.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PrimeXMTestConsoleApp.Logic
{
    public class MagicAggregator : IMagicAggregator
    {
        public IEnumerable<T> Aggregate<T, TKey>(IEnumerable<T> data,
                                                        Func<T, TKey> keySelector,
                                                        Action<T, TKey> groupActions,
                                                        Func<T, int> sumSelector,
                                                        Action<T, int> sumAction,
                                                        Func<T, double> avgSelector,
                                                        Action<T, double> avgAction)
            where T : new()
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var groupBy = data.GroupBy(keySelector);

            return groupBy.Select(g =>
            {
                var newObject = new T();
                groupActions(newObject, g.Key);

                var sum = g.Sum(sumSelector);
                sumAction(newObject, sum);

                var avg = g.Average(avgSelector);
                avgAction(newObject, avg);

                return newObject;
            });
        }

        public IEnumerable<Symbol> SymbolAggregate(IEnumerable<Symbol> data)
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            return data.GroupBy(d => d.Name)
                       .Select(x => new Symbol
                       {
                           Id = default,
                           Name = x.Key,
                           Price = x.Average(x => x.Price),
                           Quantity = x.Sum(x => x.Quantity)
                       });
        }
    }
}
