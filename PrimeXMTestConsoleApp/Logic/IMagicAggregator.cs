﻿using PrimeXMTestConsoleApp.Types;
using System;
using System.Collections.Generic;

namespace PrimeXMTestConsoleApp.Logic
{
    public interface IMagicAggregator
    {
        IEnumerable<T> Aggregate<T, TKey>(IEnumerable<T> data, 
                                          Func<T, TKey> keySelector, 
                                          Action<T, TKey> groupActions, 
                                          Func<T, int> sumSelector, 
                                          Action<T, int> sumAction, 
                                          Func<T, double> avgSelector, 
                                          Action<T, double> avgAction) 
            where T : new();
        IEnumerable<Symbol> SymbolAggregate(IEnumerable<Symbol> data);
    }
}